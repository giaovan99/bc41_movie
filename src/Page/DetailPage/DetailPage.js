import { Progress } from "antd";
import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { movieServ } from "../../service/movieService";

export default function DetailPage() {
  let params = useParams();
  const [movie, setMovie] = useState({});
  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let result = await movieServ.getDetailMovie(params.id);
        setMovie(result.data.content);
        //
      } catch (error) {}
    };
    fetchDetail();
  }, []);
  return (
    <div className="container flex">
      <img className="w-1/4" src={movie.hinhAnh} alt="" />
      <div className="p-5 space-y-10">
        <h2 className="text-xl font-medium">{movie.tenPhim}</h2>
        <p className="text-xs text-gray-600">{movie.moTa}</p>
        <Progress
          strokeColor={{
            "0%": "#108ee9",
            "100%": "#87d068",
          }}
          format={(percent) => `${percent / 10} Điểm`}
          type="circle"
          percent={movie.danhGia * 10}
        />
        <NavLink
          to={`/booking/${movie.maPhim}`}
          className="px-5 py-2 rounded bg-red-500 text-white"
        >
          Mua vé
        </NavLink>
      </div>
    </div>
  );
}
// progress antd
